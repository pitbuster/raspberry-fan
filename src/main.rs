use anyhow::{anyhow, Context, Result};
use clap::Parser;
use knuffel::{parse, Decode};
use rust_gpiozero::LED;
use systemstat::Platform;

#[derive(Decode)]
struct Config {
    #[knuffel(child)]
    interval: Interval,
    #[knuffel(child)]
    temperature: Temperature,
    #[knuffel(child)]
    pin: Pin,
}

#[derive(Decode)]
struct Interval {
    #[knuffel(argument)]
    seconds: u8,
}

#[derive(Decode)]
struct Temperature {
    #[knuffel(argument)]
    treshold_low: u8,
    #[knuffel(argument)]
    treshold_high: u8,
}

#[derive(Decode)]
struct Pin {
    #[knuffel(argument)]
    number: u8,
}

static PATHS: &[&str] = &["config.kdl", "/etc/raspberry-fan/config.kdl"];
fn read_config() -> Result<Config> {
    for file in PATHS {
        match std::fs::read_to_string(file) {
            Ok(config) => match parse::<Config>(file, &config) {
                Ok(config) => return Ok(config),
                Err(e) => {
                    println!("{:?}", miette::Report::new(e));
                    std::process::exit(1);
                }
            },
            Err(_) => continue,
        }
    }
    Err(anyhow!(format!(
        "Couldn't find config files in: {}",
        PATHS.join(",")
    )))
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Print debug messages.
    #[arg(short, long, default_value_t = false)]
    debug: bool,
}

fn main() -> Result<()> {
    let args = Args::parse();
    let config = read_config().context("Couldn't read config")?;
    let sys = systemstat::System::new();
    let fan = LED::new(config.pin.number);
    let sleep_duration = std::time::Duration::from_secs(config.interval.seconds as u64);

    loop {
        let temp = sys.cpu_temp()?;
        if !fan.is_active() && temp >= config.temperature.treshold_high as f32 {
            fan.on();
        } else if fan.is_active() && temp < config.temperature.treshold_low as f32 {
            fan.off();
        }
        if args.debug {
            println!(
                "Temp = {temp}°C\tFan = {}",
                if fan.is_active() { "ON" } else { "OFF" }
            );
        }
        std::thread::sleep(sleep_duration);
    }
}
