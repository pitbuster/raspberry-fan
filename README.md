# raspberry-fan

Controls a fan attached to a Raspberry Pi. You can follow the schematics from [this
article](https://fizzy.cc/raspberry-pi-fan/).

For the pin numbering reference, you can follow [pinout.xyz](https://pinout.xyz/).

The program will search for its configuration file (`config.kdl`) in the current
directory first (useful for development) and then in `/etc/raspberry-fan/config.kdl`
(useful for distribution packages).

You may need super user permissions to access the GPIO devices, so you may want to
`cargo build` and then run the binary from `target/debug/raspberry-fan` with `sudo`.

An example systemd service file is provided in `raspberry-fan.service`.

## Installation
On Arch Linux, you can use the [AUR
package](https://aur.archlinux.org/packages/raspberry-fan).
